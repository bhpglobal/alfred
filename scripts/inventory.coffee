# Description:
#   Script para interactuar con el inventario de BHP
#
# Dependencies:
#   None
#
# Configuration:
#   HUBOT_INVY_URL
#   HUBOT_INVY_APIKEY
#
# Commands:
#   inv all - Muestra todo el inventario
#   inv asignado a [usuario] - Muestra recursos asigandos para el usuario especificado
#   inv asignar [id] a [usuario] - Asigna el recurso a el usuario especificado
#   inv [id] - Muestra detalles para el recurso especificado
#
# Author:
#   rcasares

module.exports = (robot) ->

  req = (msg, method,where,data,cb)->
    resource = process.env.HUBOT_INVY_URL + "/api/" + where
    r = msg.http(resource)
      .header('Accept', 'application/json')
      .header('x-apikey', process.env.HUBOT_INVY_APIKEY)
    
    if data
      data = JSON.stringify(data);
      console.log data
    
    switch method
      when 'GET','DELETE'
        r.get() (err, res, body) ->
          cb JSON.parse(body)
        break
      when 'POST'
        r.post(data) (err, res, body) ->
          cb JSON.parse(body)
        break
      when 'PUT'
        r.put(data) (err, res, body) ->
          cb JSON.parse(body)
        break
      when 'PATCH'
        r.patch(data) (err, res, body) ->
          cb JSON.parse(body)

  robot.respond /inv all/i, (msg) ->
    req msg, 'GET', 'devices?expand=category&order_by=updated_at&sort=desc', "", (devices) ->
      if devices.error
        msg.send "Error: #{devices.error}"
      else
        for device in devices
          do (device) ->
            message = "ID #{device.id}, #{device.brand} #{device.model}, Categoría: #{device.category.name}, " +
                      "Nº Serie: #{device.serial}, Asignado a: #{device.assigned_to}"
            msg.send message

  robot.respond /inv asignado a ([a-z]+)/i, (msg) ->
    req msg, 'GET', "devices?assigned_to=#{msg.match[1]}&expand=category&order_by=updated_at&sort=desc", "", (devices) ->
      if devices.error
        msg.send "Error: #{devices.error}"
      else
        for device in devices
          do (device) ->
            message = "ID #{device.id}, #{device.brand} #{device.model}, Categoría: #{device.category.name}, " +
                      "Nº Serie: #{device.serial}, Asignado a: #{device.assigned_to}"
            msg.send message

  robot.respond /inv asignar ([0-9]+) a ([a-z]+)/i, (msg) ->
    req msg, 'PATCH', "devices/#{msg.match[1]}", { "assigned_to": msg.match[2] }, (device) ->
      if device.error
        msg.reply "Error: #{device.error}"
      else
        msg.reply "ID #{device.id}, asignado a: #{device.assigned_to}"


  robot.respond /inv ([0-9]+)/i, (msg) ->
    req msg, 'GET', "devices/#{msg.match[1]}", "", (device) ->
      if device.error
        msg.send "Error: #{device.error}"
      else
        msg.send "Categoría: #{device.category.name}"
        msg.send "Marca: #{device.brand}"
        msg.send "Modelo: #{device.model}"
        msg.send "Asignado a: #{device.assigned_to}"
        msg.send "Ubicación: #{device.location.address}, #{device.location.name}"
        msg.send "Fecha de adquisición: #{device.acquired_on}"
        msg.send "Expiración de garantía: #{device.acquired_on}"
        msg.send "Nº de serie: #{device.serial}"
        msg.send "Valor: $#{device.cost}"
        msg.send "Nº de factura: #{device.bill_id}"
        msg.send "Proveedor: #{device.provider.name}, Tel: #{device.provider.phone}, Email: #{device.provider.email}"
        msg.send "Componentes:"
        for component in device.components
          do (component) ->
            msg.send "[#{component.name} = #{component.value}]"