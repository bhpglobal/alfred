# Description:
#   Assign roles to people you're chatting with
#
# Commands:
#   alfred escuchate este - Contale un chiste a alfred que tiene sentido del humor
#   alfred contate uno - Alfred se cuenta un chiste
#   alfred contate otro - Alfred se cuenta otro
#   alfred olvidate esos chistes - Alfred se olvida de esos chistes malos que le enseñaron
#
# Examples:
#   alfred escuchate este: Había una vez, truz
#   alfred contate uno

respuestas = [
  'jajaja, bue-ní-si-mo!',
  'jaaaa, de donde lo sacaste?',
  'Y el chiste?',
  'Sos un desastre contando chistes!',
  'Dedicate a otra cosa'
]

module.exports = (robot) ->
  
  robot.brain.data.chistes = robot.brain.data.chistes || []

  robot.respond /escuchate este (.+)/i, (msg) ->
    robot.brain.data.chistes.push msg.match[1]
    msg.send respuestas[Math.floor(Math.random()*respuestas.length)]

  robot.respond /cuales te sabes/i, (msg) ->
    if robot.brain.data.chistes and robot.brain.data.chistes.length is 0
      msg.send "No me se ninguno, contame uno!"
    else
      for chiste in robot.brain.data.chistes
        do (chiste)->
          msg.send chiste

  robot.respond /(contate uno|contate otro)/i, (msg) ->
    if robot.brain.data.chistes.length == 0
      msg.send "No me se ninguno, contame uno!"
    else
      msg.send robot.brain.data.chistes[Math.floor(Math.random()*robot.brain.data.chistes.length)]

  robot.respond /(olvidate esos chistes)/i, (msg) ->
    robot.brain.data.chistes = []
    msg.send "Si, son malisimos..."