# Description:
#   Alfred will tell you how he feel
#
# Commands:
#   alfred cómo estas?                  - Alfred te va a decir cómo se siente.
#   alfred buen finde                   - Alfred te deseará buen fin de semana.
#   alfred llegas tarde                 - Alfred se excusa
#   alfred reinicia bhp-cloud.com       - Alfred se reinicia
#
# Examples:
#   alfred cómo estas?
# Author: ISebriano

respuestasM = [
  'muy bien!',
  'ehmmm...qué necesitas?! ',
  'todo viento y vos, qué acelga?',
  'cómo saberlo Marge, cóoomo sabeerrlo!?.. ah no, ese era otro, todo bien aquí!',
  'queti!'
]

respuestaLT = [
    'un BOT nunca llega tarde ni temprano Frodo Baggins, llega precisamente cuando quiere!. Si, llegue tarde sory.'
]

module.exports = (robot) ->

robot.respond /cómo estas/i, (msg) ->
    msg.send respuestasM[Math.floor(Math.random()*respuestasM.length)]