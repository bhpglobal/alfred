# Description:
#   Script para grabar conversaciones y luego las enviarlas por mail.
#   Ideal para minutas o daily meetings.
#
# Dependencies:
#   "nodemailer": ">=0.6.0"
#
# Configuration:
#   HUBOT_SMTP_HOST
#   HUBOT_SMTP_PORT
#   HUBOT_SMTP_USER
#   HUBOT_SMTP_PASS
#
# Commands:
#   alfred grabar - Comienza a grabar la conversación
#   alfred estas grabando? - Te dice si está grabando o no
#   alfred detener - Detiene la grabación
#   alfred que grabaste? - Te muestra lo que se grabó
#   alfred mandalo a [email] - Manda la conversación por email, pueden ser varios emails separados por espacios
#   alfred borralo - Cancela la grabación y la borra
#
# Author:
#   rcasares

mailer = require 'nodemailer'

module.exports = (robot) ->
  
  robot.brain.data.recordings = []

  currentDate = () ->
    today = new Date()
    d = today.getDate()
    m = today.getMonth() + 1
    y = today.getFullYear()
    h = today.getHours()
    min = today.getMinutes()
    today = "#{d}/#{m}/#{y} #{h}:#{min}"
    today

  sendEmail = (recipients, subject, msg, from, cb) ->
    transport = mailer.createTransport("SMTP",{
      host: process.env.HUBOT_SMTP_HOST,
      port: process.env.HUBOT_SMTP_PORT,
      auth: {
        user: process.env.HUBOT_SMTP_USER,
        pass: process.env.HUBOT_SMTP_PASS
      }
    });

    mail = {
      from: "Alfred <#{process.env.HUBOT_SMTP_USER}>",
      to: "#{recipients}",
      subject: subject,
      text: "#{msg}"
    }

    transport.sendMail mail, (error, response) ->
      if error
        cb { "error": error }
      else
        cb { "msg": response.message }

  robot.respond /grabar/i, (msg) ->
    if robot.brain.data.recordings[msg.message.room]?
      if robot.brain.data.recordings[msg.message.room].recording
        msg.reply "Ya estoy grabando!"
        return

    robot.brain.data.recordings[msg.message.room] = {}
    robot.brain.data.recordings[msg.message.room].recording = true
    robot.brain.data.recordings[msg.message.room].messages = []
    msg.reply "Ok, ahora estoy grabando"

  robot.respond /detener/i, (msg) ->
    robot.brain.data.recordings[msg.message.room]?.recording = false
    msg.reply "Listo!"

  robot.respond /estas grabando?/i, (msg) ->
    if robot.brain.data.recordings[msg.message.room]?.recording
      msg.reply "Si!"
    else
      msg.reply "No!"

  robot.respond /que grabaste?/i, (msg) ->
    if robot.brain.data.recordings[msg.message.room]?.messages.length
      for message in robot.brain.data.recordings[msg.message.room].messages
        do (message) ->
          msg.send message
    else
      msg.reply "No he grabado nada"

  robot.respond /borralo/i, (msg) ->
    robot.brain.data.recordings[msg.message.room]?.messages = []
    msg.reply "Borrado!"

  robot.respond /mandalo a (.*)/i, (msg) ->
    unless robot.brain.data.recordings[msg.message.room]?.messages.length
      msg.reply "No tengo nada para mandar"
      return

    msg.reply "Envianding..."
    message = robot.brain.data.recordings[msg.message.room].messages.join("\n")
    date = currentDate()
    sendEmail msg.match[1].split(" "), "#{msg.message.user.name} te envió una grabación el día #{date}", message, msg.message.user.name, (res) ->
      if res.error
        msg.reply "Uooooops: #{res.error}!"
      else
        msg.reply "Listo! #{res.msg}"

  robot.catchAll (msg) ->
    if robot.brain.data.recordings[msg.message.room]?.recording
      robot.brain.data.recordings[msg.message.room].messages.push "#{msg.message.user.name}: #{msg.message.text}"