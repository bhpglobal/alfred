module.exports = (robot) ->
  robot.router.post '/hubot/mailchatter/:room', (req, res) ->
    room = req.params.room
    message = req.body

    robot.messageRoom room, "@here #{message.headers.Subject} [#{message.headers.From}]"
    robot.messageRoom room, "#{message.plain}"
    res.status(201).send message